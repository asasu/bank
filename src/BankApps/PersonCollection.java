/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankApps;

import java.util.Hashtable;

/**
 *
 * @author dequansa
 */
public class PersonCollection 
{
     private Persons customer;
     private int PersonID =1000;
     private Hashtable<Integer, Persons> customerCollection = new Hashtable<Integer, Persons>();
     
     
     public int addPerson(String _fname, String _lname)
     {
         PersonID++;
         customer = new Persons(_fname,_lname,PersonID);
         customerCollection.put(PersonID, customer);
         return PersonID;
     }
     
     public Persons get_customer(Integer PersonKey)
     {
         if ( customerCollection.containsKey(PersonKey))
         {
            customer = customerCollection.get(PersonKey);
            return customer;
         }
         else
         {
             return null;
         }
     }
     public int get_collection_length()
     {
         return customerCollection.size();
     }
     
     
     
     
}
