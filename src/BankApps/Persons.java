/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankApps;

/**
 *
 * @author dequansa
 */


public class Persons 
{
    private String FristName = "";
    private String LastName = "";
    private int personID = 0;
    
    
    public Persons (String _fname, String _lname, int _personid)
    {
        FristName = _fname;
        LastName = _lname;
        personID = _personid;
    }
    
    public int get_PersonID()
    {
        return personID;
    }
    public String get_FristName()
    {
        return FristName;
    }
    public String get_LastName ()
    {
        return LastName;
    }
    public String get_personID ()
    {
        return LastName;
    }
    
    public String get_FullName ()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(FristName);
        sb.append(" ");
        sb.append(LastName);
        return sb.toString();
    }
    
    public void set_FristName(String _fname)
    {
        FristName = _fname;
    }
    public void set_LastName (String _lname)
    {
        LastName = _lname;
    }
    
}
